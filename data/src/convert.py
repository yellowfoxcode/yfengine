import json

# Assuming your JSON data is stored in a variable called 'json_data'
with open('chart.json') as i:
    json_data = json.load(i)

# Loop through each note in the 'notes' array
for note in json_data['song']['notes']:
    # Loop through each sectionNote array
    for sectionNote in note['sectionNotes']:
        # Decrement the first value by 100
        sectionNote[0] += 50

# Output the modified JSON
out_file = open("chartNew.json", "w")
   
json.dump(json_data, out_file, indent = 2)
   
out_file.close()